module golp

go 1.12

require (
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/olekukonko/tablewriter v0.0.1
	github.com/stretchr/testify v1.4.0
)
