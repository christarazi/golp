package logentry

import (
	"regexp"
	"sort"
	"strings"
	"time"
)

const NginxRegularExpression = "(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}) - - " +
	"\\[(\\d{1,2}\\/\\w{3}\\/\\d{4}):(\\d{2}:\\d{2}:\\d{2} [-\\+]\\d{4}).+" +
	"(\"(GET|POST|HEAD) (\\/.*) (HTTP\\/\\d\\.\\d)\" (\\d{3}) (\\d{1,5}) \".+\" \"(.+)\")"

// Represents an HTTP request log entry.
type request struct {
	Method       string
	Endpoint     string
	HTTPVersion  string
	ResponseCode string
	Reserved     string
	UserAgent    string
}

// Represents a general log entry.
type LogEntry struct {
	IP        string
	Date      string
	Time      string
	Timestamp time.Time
	Action    string
	Request   request
}

func CreateEntry(ip, date, timestr, action, method,
	endpoint, httpv, rescode, resv, uastr []byte) LogEntry {
	_date := string(date)
	_timestr := string(timestr)
	dt := _date + " " + _timestr
	ts, _ := time.Parse("02/Jan/2006 15:04:05 -0700", dt)

	return LogEntry{
		IP:        string(ip),
		Date:      string(date),
		Time:      string(timestr),
		Timestamp: ts,
		Action:    string(action),
		Request: request{
			Method:       string(method),
			Endpoint:     string(endpoint),
			HTTPVersion:  string(httpv),
			ResponseCode: string(rescode),
			Reserved:     string(resv),
			UserAgent:    string(uastr),
		},
	}
}

func Parse(content [][]byte) ([]LogEntry, [][]byte) {
	var matches []LogEntry
	var nonmatches [][]byte

	regex, _ := regexp.Compile(NginxRegularExpression)
	for _, v := range content {
		if len(v) == 0 {
			continue
		}

		if submatches := regex.FindAllSubmatch(v, -1); submatches != nil {
			sb := submatches[0]

			ip := sb[1]
			date := sb[2]
			time := sb[3]
			action := sb[4]
			method := sb[5]
			endpoint := sb[6]
			httpv := sb[7]
			rescode := sb[8]
			resv := sb[9]
			uastr := sb[10]

			matches = append(matches,
				CreateEntry(ip, date, time, action, method, endpoint, httpv, rescode, resv, uastr))
		} else {
			nonmatches = append(nonmatches, v)
		}
	}

	return matches, nonmatches
}

// Filter filters entries if filter is found in the UserAgent field. If filter
// is found within the UserAgent, it is ignored. Entries that are considered
// "filtered" DO NOT contain the filter substring within the UserAgent field.
func Filter(entries []LogEntry, filter string) ([]LogEntry, []LogEntry) {
	var filtered, ignored []LogEntry

	for _, v := range entries {
		if strings.Contains(v.Request.UserAgent, filter) {
			ignored = append(ignored, v)
		} else {
			filtered = append(filtered, v)
		}
	}

	return filtered, ignored
}

func Sort(m []LogEntry) {
	sort.Sort(byTimestamp(m))
}

// Methods for sort.Interface.
type byTimestamp []LogEntry

func (t byTimestamp) Len() int {
	return len(t)
}

func (t byTimestamp) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}

func (t byTimestamp) Less(i, j int) bool {
	return t[i].Timestamp.Unix() < t[j].Timestamp.Unix()
}

// End of sort.Interface.
