package logentry

import (
	"bytes"
	"io/ioutil"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

const (
	ExpectedMatches    = 8
	ExpectedNonmatches = 2
)

var data []byte

func TestParse(t *testing.T) {
	lines := bytes.Split(data, []byte("\n"))
	assert.NotEqual(t, len(lines), 0)

	matches, nonmatches := Parse(lines)

	assert.Equal(t, len(matches), ExpectedMatches)
	assert.Equal(t, len(nonmatches), ExpectedNonmatches)

	expectedEntries := []LogEntry{
		{
			IP:     "95.213.130.90",
			Date:   "08/Apr/2018",
			Time:   "07:54:55 -0400",
			Action: "\"GET /_asterisk/ HTTP/1.1\" 404 136 \"-\" \"python-requests/2.18.4\"",
			Request: request{
				Method:       "GET",
				Endpoint:     "/_asterisk/",
				HTTPVersion:  "HTTP/1.1",
				ResponseCode: "404",
				Reserved:     "136",
				UserAgent:    "python-requests/2.18.4",
			},
		},

		{
			IP:     "184.105.139.70",
			Date:   "08/Apr/2018",
			Time:   "09:26:24 -0400",
			Action: "\"GET / HTTP/1.1\" 200 3997 \"-\" \"-\"",
			Request: request{
				Method:       "GET",
				Endpoint:     "/",
				HTTPVersion:  "HTTP/1.1",
				ResponseCode: "200",
				Reserved:     "3997",
				UserAgent:    "-",
			},
		},

		{
			IP:     "216.218.206.66",
			Date:   "08/Apr/2018",
			Time:   "10:24:07 -0400",
			Action: "\"GET / HTTP/1.1\" 200 3997 \"-\" \"-\"",
			Request: request{
				Method:       "GET",
				Endpoint:     "/",
				HTTPVersion:  "HTTP/1.1",
				ResponseCode: "200",
				Reserved:     "3997",
				UserAgent:    "-",
			},
		},

		{
			IP:     "185.234.15.88",
			Date:   "08/Apr/2018",
			Time:   "10:27:23 -0400",
			Action: "\"GET /xmlrpc.php HTTP/1.1\" 301 178 \"-\" \"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6\"",
			Request: request{
				Method:       "GET",
				Endpoint:     "/xmlrpc.php",
				HTTPVersion:  "HTTP/1.1",
				ResponseCode: "301",
				Reserved:     "178",
				UserAgent:    "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6",
			},
		},

		{
			IP:     "201.6.107.126",
			Date:   "08/Apr/2018",
			Time:   "10:33:40 -0400",
			Action: "\"GET /admin/config.php HTTP/1.1\" 404 162 \"-\" \"curl/7.15.5 (x86_64-redhat-linux-gnu) libcurl/7.15.5 OpenSSL/0.9.8b zlib/1.2.3 libidn/0.6.5\"",
			Request: request{
				Method:       "GET",
				Endpoint:     "/admin/config.php",
				HTTPVersion:  "HTTP/1.1",
				ResponseCode: "404",
				Reserved:     "162",
				UserAgent:    "curl/7.15.5 (x86_64-redhat-linux-gnu) libcurl/7.15.5 OpenSSL/0.9.8b zlib/1.2.3 libidn/0.6.5",
			},
		},

		{
			IP:     "159.203.121.40",
			Date:   "08/Apr/2018",
			Time:   "10:47:23 -0400",
			Action: "\"GET / HTTP/1.0\" 200 3997 \"-\" \"Mozilla/5.0 (compatible; NetcraftSurveyAgent/1.0; +info@netcraft.com)\"",
			Request: request{
				Method:       "GET",
				Endpoint:     "/",
				HTTPVersion:  "HTTP/1.0",
				ResponseCode: "200",
				Reserved:     "3997",
				UserAgent:    "Mozilla/5.0 (compatible; NetcraftSurveyAgent/1.0; +info@netcraft.com)",
			},
		},

		{
			IP:     "185.234.15.88",
			Date:   "08/Apr/2018",
			Time:   "10:48:52 -0400",
			Action: "\"GET /xmlrpc.php HTTP/1.1\" 301 178 \"-\" \"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6\"",
			Request: request{
				Method:       "GET",
				Endpoint:     "/xmlrpc.php",
				HTTPVersion:  "HTTP/1.1",
				ResponseCode: "301",
				Reserved:     "178",
				UserAgent:    "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6",
			},
		},

		{
			IP:     "5.79.69.140",
			Date:   "08/Apr/2018",
			Time:   "11:09:40 -0400",
			Action: "\"GET /recordings/ HTTP/1.1\" 400 264 \"-\" \"curl/7.29.0\"",
			Request: request{
				Method:       "GET",
				Endpoint:     "/recordings/",
				HTTPVersion:  "HTTP/1.1",
				ResponseCode: "400",
				Reserved:     "264",
				UserAgent:    "curl/7.29.0",
			},
		},
	}

	for i, v := range expectedEntries {
		expectedEntries[i].Timestamp, _ = time.Parse("02/Jan/2006 15:04:05 -0700",
			v.Date+" "+v.Time)
	}

	for i, v := range matches {
		e := &expectedEntries[i]
		assert.Equal(t, *e, v)
	}
}

func TestFilter(t *testing.T) {
	lines := bytes.Split(data, []byte("\n"))
	assert.NotEqual(t, len(lines), 0)

	matches, _ := Parse(lines)

	filtered, ignored := Filter(matches, "curl")

	assert.Equal(t, len(filtered), 6)
	assert.Equal(t, len(ignored), 2)
}

func init() {
	var err error
	data, err = ioutil.ReadFile("../fixtures/access.log")
	if err != nil {
		panic("error reading fixtures file: " + err.Error())
	}
}
