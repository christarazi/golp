package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"net"
	"os"
	"time"

	"golp/logentry"

	tablewriter "github.com/olekukonko/tablewriter"
)

func main() {
	args := parseArgs()

	if isFlagPassed("trim") && isFlagPassed("verbose") {
		fatalf("cannot pass both -trim and -verbose\n")
	}

	stdin, err := isStdin()
	if err != nil {
		fatalf("error checking for stdin: %v\n", err)
	}

	if *args.file == "" && !stdin {
		flag.Usage()
		os.Exit(1)
	}

	var file *os.File

	if (*args.file == "" || *args.file == "-") && stdin {
		file = os.Stdin
	} else {
		file, err = os.Open(*args.file)
		if err != nil {
			fatalf("error opening '%s': %v\n", *args.file, err)
		}
	}

	data, err := read(file)
	if err != nil {
		fatalf("error reading '%s': %v\n", *args.file, err)
	}

	if len(data) == 0 {
		return
	}

	lines := bytes.Split(data, []byte("\n"))
	m, n := logentry.Parse(lines)

	var fi, ig []logentry.LogEntry
	if *args.filter != "" {
		fi, ig = logentry.Filter(m, *args.filter)

		logentry.Sort(fi)
	} else {
		logentry.Sort(m)
	}

	if *args.verbose {
		fmt.Printf("Matches:     %v\n"+
			"Filtered:    %v\n"+
			"Ignored:     %v\n"+
			"Nonmatches:  %v\n"+
			"Total lines: %v\n\n", len(m), len(fi), len(ig), len(n), len(lines))
	}

	if *args.filter != "" {
		printTable(args, fi)
	} else {
		printTable(args, m)
	}

	if *args.verbose {
		fmt.Print("Unmatched lines:\n\n")
		for _, v := range n {
			fmt.Println(string(v))
		}
	}
}

func printTable(args arguments, entries []logentry.LogEntry) {
	outputTable := tablewriter.NewWriter(os.Stdout)
	outputTable.SetAutoMergeCells(true)
	outputTable.SetRowLine(true)

	headers := []string{"Timestamp"}
	if *args.reverseip {
		headers = append(headers, "Hostname")
	} else {
		headers = append(headers, "IP")
	}
	headers = append(headers, []string{"Method", "Endpoint", "UA"}...)
	outputTable.SetHeader(headers)

	table := make([][]string, 0, len(entries))

	for _, entry := range entries {
		var tmp []string

		if *args.localtime {
			tmp = append(tmp, entry.Timestamp.Local().Format(time.RFC822Z))
		} else {
			tmp = append(tmp, entry.Timestamp.Format(time.RFC822Z))
		}

		if *args.reverseip {
			names, err := net.LookupAddr(entry.IP)
			if err != nil {
				tmp = append(tmp, entry.IP)
			} else {
				tmp = append(tmp, names[0])
			}
		} else {
			tmp = append(tmp, entry.IP)
		}

		var endpoint string
		if isFlagPassed("trim") {
			endpoint = trimLength(entry.Request.Endpoint, *args.trim)
		} else {
			endpoint = entry.Request.Endpoint
		}

		tmp = append(tmp,
			entry.Request.Method,
			endpoint,
			entry.Request.UserAgent)

		table = append(table, tmp)
	}

	outputTable.AppendBulk(table)
	outputTable.Render()
}

func trimLength(s string, n int) string {
	if len(s) > n {
		return s[:n] + "...(continued)"
	}

	return s
}

func fatalf(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, format, args...)
	os.Exit(1)
}

func isStdin() (bool, error) {
	stat, err := os.Stdin.Stat()
	if err != nil {
		return false, err
	}

	return (stat.Mode() & os.ModeCharDevice) == 0, nil
}

type arguments struct {
	file      *string
	filter    *string
	group     *string
	localtime *bool
	reverseip *bool
	trim      *int
	verbose   *bool
}

func parseArgs() arguments {
	// Set command line arg flags.
	fi := flag.String("file", "", "log file to analyze/parse")
	fl := flag.String("filter", "",
		"filter out entries by UserAgent (example: exclude \"Googlebot\" UserAgents)")
	gr := flag.String("group", "IP", "category to group entries by")
	lt := flag.Bool("localtime", false, "convert timestamp into local time")
	ri := flag.Bool("reverse-ip", false, "try to reverse ip addr to hostnames")
	tr := flag.Int("trim", trimLengthAmount, "trim endpoint text to a certain length")
	vb := flag.Bool("verbose", false, "print unmatched lines as well")
	flag.Parse()

	return arguments{
		file:      fi,
		filter:    fl,
		group:     gr,
		localtime: lt,
		reverseip: ri,
		trim:      tr,
		verbose:   vb,
	}
}

const trimLengthAmount = 40

func isFlagPassed(name string) bool {
	found := false
	flag.Visit(func(f *flag.Flag) {
		if f.Name == name {
			found = true
		}
	})
	return found
}

func read(stream io.Reader) ([]byte, error) {
	var p []byte
	buf := bytes.NewBuffer(p)
	_, err := buf.ReadFrom(stream)
	return buf.Bytes(), err
}
