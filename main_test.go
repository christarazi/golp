package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRead(t *testing.T) {
	f, err := os.Open("fixtures/access.log")
	assert.Nil(t, err)

	data, err := read(f)
	assert.Nil(t, err)
	assert.NotEqual(t, len(data), 0, "Reading fixture file should not be empty")
}
